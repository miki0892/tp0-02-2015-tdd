package main.java.ar.fiuba.tdd.tp0;

import java.util.Hashtable;
import java.util.List;
import java.util.function.*;
import java.util.stream.*;

public class MultiOperation {

    private Hashtable<String,Function<List<Float>,Float>> types = new Hashtable<>();

    public MultiOperation() {

        types.put("++", this::addAll);
        types.put("--", this::substractAll);
        types.put("**", this::multiplyAll);
        types.put("//", this::divideAll);

    }

    public Float resolve(String operation, List<Float> arguments ) {
        return types.get(operation).apply(arguments);
    }

    private Float addAll(List<Float> numbers) {
        Stream<Float> arguments = numbers.stream();
        return arguments.reduce((float) 0, (acc, element) -> acc + element);
    }

    private Float substractAll(List<Float> numbers) {
        numbers.set(0,-numbers.get(0));
        Stream<Float> arguments = numbers.stream();
        return arguments.reduce((float) 0, (acc, element) -> acc - element);
    }

    private Float multiplyAll(List<Float> numbers) {
        Stream<Float> arguments = numbers.stream();
        return arguments.reduce((float) 1, (acc, element) -> acc * element);
    }

    private Float divideAll(List<Float> numbers) {
        Stream<Float> arguments = numbers.stream();
        return arguments.reduce((float) Math.pow(numbers.get(0), 2), (acc, element) -> acc / element);
    }
}
