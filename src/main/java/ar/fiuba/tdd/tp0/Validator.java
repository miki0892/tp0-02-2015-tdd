package main.java.ar.fiuba.tdd.tp0;

import java.util.Hashtable;
import java.util.function.*;


public class Validator {

    private Hashtable<Boolean,Consumer<String>> expressionsValidations = new Hashtable<>();
    private Hashtable<Boolean,Consumer<String>> operatingValidations = new Hashtable<>();
    private ElementType elementType;

    public enum ElementType {
        OPERATING,
        OPERATION
    }

    public Validator() {
        expressionsValidations.put(true, (string) -> { throw new IllegalArgumentException(string); });
        expressionsValidations.put(false, (string) -> { });

        operatingValidations.put(true, (string) -> { elementType = ElementType.OPERATING; } );
        operatingValidations.put(false, (string) -> { elementType = ElementType.OPERATION; });
    }

    public void validateExpression(String expression) {
        Boolean nullValidation = expression == null;
        String messageNull = "Argument is null";
        expressionsValidations.get(nullValidation).accept(messageNull);

        assert expression != null;
        String messageInvalidArgument = expression + ": Invalid Argument";
        expressionsValidations.get(expression.isEmpty()).accept("Argument empty");
        String validation = "(^[+-/*]*$)|(^[0-9] [+-/*]$)|(^[0-9] [0-9] [^+-/*]$)";
        expressionsValidations.get(expression.matches(validation)).accept(messageInvalidArgument);

    }

    public ElementType validateOperating(String element) {
        Boolean isOperating = element.matches("^-?[0-9]+$");
        operatingValidations.get(isOperating).accept(element);
        return elementType;

    }

}
