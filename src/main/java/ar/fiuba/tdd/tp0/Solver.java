package main.java.ar.fiuba.tdd.tp0;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;
import java.util.function.*;

public class Solver {

    private Stack<Float> operatings = new Stack<>();
    private Hashtable<Validator.ElementType,Consumer<String>> resolutionElement = new Hashtable<>();
    private Hashtable<Boolean,Consumer<String>> typeOperation = new Hashtable<>();
    private SimpleOperation simpleOperation = new SimpleOperation();
    private MultiOperation multiOperation = new MultiOperation();

    public Solver() {
        resolutionElement.put(Validator.ElementType.OPERATING, this::resolveOperating);
        resolutionElement.put(Validator.ElementType.OPERATION, this::resolveOperation);

        typeOperation.put(true, this::resolveMultiOperation);
        typeOperation.put(false, this::resolveSimpleOperation);
    }

    private void resolveOperation(String operation) {
        Boolean isMultiOperation = operation.length() == 2;
        typeOperation.get(isMultiOperation).accept(operation);
    }

    private void resolveSimpleOperation(String operation) {
        float argument2 = operatings.pop();
        float resultSimpleOperation = simpleOperation.resolve(operation,operatings.pop(),argument2);
        operatings.push(resultSimpleOperation);
    }

    private void resolveMultiOperation(String operation) {
        List<Float> numbers = new ArrayList<>(operatings);
        operatings.clear();
        float resultMultiOperation = multiOperation.resolve(operation,numbers);
        operatings.push(resultMultiOperation);
    }

    private void resolveOperating(String operating) {
        float number = Float.parseFloat(operating);
        operatings.push(number);
    }

    public void resolve(String element,Validator.ElementType elementType) {
        resolutionElement.get(elementType).accept(element);
    }

    public float getResult() {
        return operatings.firstElement();
    }
}
