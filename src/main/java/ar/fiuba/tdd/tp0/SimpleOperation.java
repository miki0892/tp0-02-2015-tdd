package main.java.ar.fiuba.tdd.tp0;

import java.util.Hashtable;
import java.util.function.*;

public class SimpleOperation {

    private Hashtable<String,BiFunction<Float,Float,Float>> types = new Hashtable<>();

    public SimpleOperation() {

        types.put("+", (argument1,argument2) ->  argument1 + argument2 );
        types.put("-", (argument1,argument2) ->  argument1 - argument2 );
        types.put("*", (argument1,argument2) ->  argument1 * argument2 );
        types.put("/", (argument1,argument2) ->  argument1 / argument2 );
        types.put("MOD", (argument1,argument2) ->  argument1 % argument2 );

    }

    public float resolve(String operation, float argument1, float argument2) {
        return types.get(operation).apply(argument1,argument2);
    }
}
