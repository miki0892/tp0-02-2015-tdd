package main.java.ar.fiuba.tdd.tp0;

public class RPNCalculator {

    private Solver solver;
    private Validator validator;

    public RPNCalculator() {

        solver = new Solver();
        validator = new Validator();

    }

    public float eval(String expression) {

        validator.validateExpression(expression);

        Parser parser = new Parser();
        String[] joinExpression = parser.joinString(expression);

        for (String element : joinExpression) {
            solver.resolve(element,validator.validateOperating(element));
        }

        return solver.getResult();
    }

    public static void main(String [] args) {
        RPNCalculator calculator = new RPNCalculator();
        System.out.println("Result: " + calculator.eval("5 2 4 5 //"));
    }
}
